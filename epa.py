import time 
import numpy  as np
import mdtraj as md
import itertools
import subprocess, os
import logging
from scipy.spatial import ConvexHull


# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
#   PART 1: OBTAINING DIPOLE MOMENTS                                      *
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 


#   INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY
#   Returns a dictionary whose keys are the names of 'atomtypes',
#   and the values their charges. A fully functional Gromacs is requiered.
#   Input: (Str) Name of the Tpr File
def _charges_from_tpr(tpr_file):

    # used for redirect standard error output to DEVNULL
    FNULL = open(os.devnull, 'w')
    result = subprocess.run(['gmx', 'dump', '-s', tpr_file], stdout=subprocess.PIPE, stderr=FNULL)
    tpr_list = result.stdout.decode('utf-8').split("\n")
    names, charges = [], []

    for line in tpr_list:
        if 'atom[' in line:
            if 'q=' in line:
                charges.append(float(line.split("q=")[1].split(",")[0]))
            else:
                names.append(line.split('{name="')[1][:-2])
    return(dict(zip(names, charges)))


#   INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY
#   Returns an N-shaped array containing each charge of the N atoms in the simulation
#   It uses the Gromacs order which is the same as the used on MDTraj
#   THIS SHOULD BE THE PREFERRED FUNCTION FOR GETTING CHARGES
def _charges_array(tpr_file):
    # used for redirect standard error output to DEVNULL
    FNULL = open(os.devnull, 'w')
    result = subprocess.run(['gmx', 'dump', '-s', tpr_file, '-sys', 'yes'], stdout=subprocess.PIPE, stderr=FNULL)
    tpr_list = result.stdout.decode('utf-8').split("\n")
    charges = []

    for line in tpr_list:
        if 'atom[' in line:
            if 'q=' in line:
                charges.append(float(line.split("q=")[1].split(",")[0]))
    return(np.array(charges))


#   INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY 
#   Returns an array containing the indexes (from 0 to number of atoms in the system) of
#   the atoms  for a given residue name
def _atoms_of_residues(traj_file, top_file, residue_name):
    indexes = []
    for explore in md.iterload(traj_file, top=top_file, chunk=1):
        for idx, at in enumerate(explore.topology.atoms):
           if (at.residue.name == residue_name):
               indexes.append(idx)
        break
    return(indexes)


# Given a list of index of residues, returns the list of index of the atomos in them
def _atoms_idx_of_residues_idx(traj_file, top_file, residues_idxs):
    indexes = []
    for explore in md.iterload(traj_file, top=top_file, chunk=1): 
        for res_idx in residues_idxs:
            for atom in explore.topology.residue(res_idx).atoms:
                indexes.append(atom.index)
        break
    return(indexes)


# Given a residue name, returns the list of index of those residues
def _residues_idx_of_name(traj_file, top_file, residue_name):
    indexes = []
    for explore in md.iterload(traj_file, top=top_file, chunk=1):
        for residue in explore.topology.residues:
            if residue.name == residue_name:
                indexes.append(residue.index)
        break
    return(indexes)

            

#   INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY 
#   Given a residue name, returns the number of atoms in it  CUIDADOOOOOOOOOOOOOOOOOO
def _natoms_in_residue(traj_file, top_file, residue_name):  # OJOOOOOO ESTO PARECE ESTAR MAAAAALLLLLL
    nats_res = 0
    for explore in md.iterload(traj_file, top=top_file, chunk=1):
        for res in explore.topology.residues:
           if (res.name == residue_name):
               nats_res += 1
           return(nats_res)
    raise RuntimeError('Residue not found!')



# Takes the cartesian coordinates of a set of points
# Considers that the system is periodic and puts the points
# into a box of a given size
def fit_into_box(p, size):
    for k, q_max in enumerate(size):
        idxs = (p[:,k] <  0.)
        mult = np.trunc(  - p[:,k][idxs] / q_max ) + 1.
        p[:,k][idxs] += mult * q_max
        idxs = (p[:,k] >=  q_max)
        mult = np.trunc(  p[:,k][idxs] / q_max ) 
        p[:,k][idxs] -= mult * q_max
    return(p) 


def distance_pbc(points, size):
    points = fit_into_box(points, size)
    d = np.empty((points.shape[0], points.shape[0], points.shape[1]) ) #reemplazar por empty una vez que ande
    for k, q_max in enumerate(size):
        tmp      = np.reshape(points[:,k], (points.shape[0], 1)) 
        d[:,:,k] = tmp - tmp.T
        idxs     = (d[:,:,k] > q_max * 0.5)
        d[:,:,k][idxs] -= q_max
        idxs     = (d[:,:,k] <= -q_max * 0.5)
        d[:,:,k][idxs] += q_max
    return(d)


def geometric_center_pbc(points, size):
    # Using algorithm from
    # https://en.wikipedia.org/wiki/Center_of_mass#Systems_with_periodic_boundary_conditions
    points = fit_into_box(points, size)
    gc = np.zeros(len(size))
    for k, q_max in enumerate(size):
        a = np.mean( np.cos( points[:,k]*(2.*np.pi/q_max) ) )
        b = np.mean( np.sin( points[:,k]*(2.*np.pi/q_max) ) )
        mean_theta = np.arctan2(-b, -a) + np.pi
        gc[k] = q_max * mean_theta / (2.*np.pi)
    return(gc)


# Return a Scipy ConvexHull object optimized due periodic boundary conditions
def convex_hull_pbc(points, size):
    points = fit_into_box(points, size)
    gc     = geometric_center_pbc(points, size)   
    points[:,0] -= gc[0]+ size[0]/2
    points[:,1] -= gc[1]+ size[1]/2
    points[:,2] -= gc[2]+ size[2]/2
    points = fit_into_box(points, size)
    return(ConvexHull(points))



# Calcula las envolventes convexas para una capa que diste en menos que una cierta 'distance' para una trayectoria
# 'chem_specie' es la especie que determina la capa y de la que se especifan las distancias en 'distance_file'
# Para determinar bien el centroide (ver funcion geometric_center_pbc ) suele convenir incluir todo lo que esté dentro de la capa
# si la capa esta solvatando algo (rodeándolo), conviene incluir ese algo. Como la simulacion suele ser solvente + agua, incluyo toda otra especie
# (que suele ser una proteina), ojo con esto si hay otras especies fuera de la capa (ej iones). En cuyo caso, removerlos con "remove_from_inside" (aplica luego de include_inside, por lo que si se incluye a ese residuo en ambas listas (include_inside y remove_from_inside) el residuo no será considerado)
# No tiene mucho sentido calcular los volúmenes de toooodos los pasos, ya que debería ser bastatne constante, entonces se calcula cada "skip" pasos
# que si no se especifica será igual al "chunk" (un volumen por chunk). 

def shell_convex_volumes(traj_file, top_file, tpr_file, distance_file, distance, chem_specie, include_inside = "everything", remove_from_inside = None, skip = None, chunk = 500):
    # Load the distance file
    distances = np.load(distance_file, mmap_mode='r')
    # Set the skip value if has not been already setted
    if skip == None:
        skip = chunk

    # Number of frames in trajectory
    aux = search_setting(tpr_file,['nsteps', 'nstxout-compressed'])
    number_of_frames = int(aux[0]/aux[1]) 
    del(aux)

    last_frame = number_of_frames - 1 

   # First list the names residues of interest (except those from the shell (usualy water))
    for explore in md.iterload(traj_file, top=top_file, chunk=1):
        # List of every residues names
        residues_names = []
        for residue in explore.topology.residues:
            if residue.name not in residues_names:
                residues_names.append(residue.name)

        if include_inside == "everything":
            residues_included = residues_names[:]
        else:
            if isinstance(include_inside, str):
                residues_included =  [include_inside]
            elif isinstance(include_inside, list) or isinstance(include_inside, tuple):
                residues_included = list(include_inside)[:]
            else:
                raise RuntimeError("'include_inside' argument must be a list, tuple or")

        # Removing residues names of the list of considered residues
        if remove_from_inside == None:
            break
        if isinstance(remove_from_inside, str): # If it is an string
            if remove_from_inside in residues_included:
                residues_included.remove(remove_from_inside)
            break
        for residue in remove_from_inside: # If is another iterable (list, tuples ...)
            if residue in residues_included:
                residues_included.remove(residue)
            else:
                print("Warning, residue ",+residue+" was listed for remotion but is not included (it is not necesary wrong)")
        break
    if chem_specie in residues_included:
        residues_included.remove(chem_specie)

    # There will be two sets. One set of atoms always included for computed the convex hull, and other set in which varies
    # step to step, containing only those (often) solvent molecules to considere (those inner than certain distance)
    atoms_always_in = []
    for residue_name in residues_included:
        atoms_always_in += _atoms_of_residues(traj_file, top_file, residue_name)
    atoms_always_in = np.array(sorted(atoms_always_in))
    res_idx_cs = np.array(_residues_idx_of_name(traj_file, top_file, chem_specie))


    volumes = list()
    n_atoms = list()

    block_iteration = 1
    for block in md.iterload(traj_file, top=top_file, chunk=chunk, skip=skip):
        first_processed_frame = (block_iteration - 1) * chunk
        last_processed_frame  = min(block_iteration * chunk - 1, number_of_frames - 1)
        items = np.arange(first_processed_frame, min(last_processed_frame, last_frame) + 1 )
        frames_included  = items[np.where(items % skip == 0)]
        frames_included_block_ref = frames_included - (block_iteration - 1) * chunk

        block_iteration += 1

        for idx, frame in enumerate(frames_included):
            print("Frame:  ", frame)
            solvate_included_res = res_idx_cs[ np.where(distances[frame,:]<distance)[0] ]
            atoms_acepted_solv   = np.array( _atoms_idx_of_residues_idx(traj_file, top_file, solvate_included_res) )
            if atoms_acepted_solv.size == 0:
                atoms_acepted = atoms_always_in[:]
            else:
                atoms_acepted = np.sort(np.concatenate( (atoms_always_in, atoms_acepted_solv) ))

            points = block.xyz[frames_included_block_ref[idx], atoms_acepted, :]#[0,:,:]
            size   = block.unitcell_lengths[frames_included_block_ref[idx]]
            hull = convex_hull_pbc(points, size)
 
            n_atoms.append(len(atoms_acepted_solv))
            volumes.append(hull.volume)
    return(np.array(volumes), np.array(n_atoms))
    '''            
        FILTRO POR DISTANCIA LOS RESIDUOS DE AGUA QUE ESTÁN A MENOS DE X nm obteniendo los indices
        ESOS INDICES ME DAN LOS RESIDUOS CUANDO LOS METO EN res_idx_cs[[ESTOSINDICES]]
        CON LOS INDICES DE LOS RESIDUOS CONSIGO LOS INDICES DE LOS ATOMOS CON _atoms_idx_of_residues_idx
        CON ESOS INDICES MAS LOS INDICES FIJOS DE atoms_always_in OBTENGO LOS PUNTOS DEL xyz
        CON ESO SACO LA ENVOLVENTE CONVEXA FRAME A FRAME, LE SACO EL VOLUMEN Y LO PONGO EN UNA LISTA
    '''
    return



''' Search for values setted in the mdp Gromac's file
    Input : tpr_file name (str)
    Output: list of values required
'''
def search_setting(tpr_file, substrlist = ['dt', 'nsteps', 'init-step', 'nstxout-compressed']):
    # Used for redirect standard error output to DEVNULL
    FNULL = open(os.devnull, 'w')
    result = subprocess.run(['gmx', 'dump', '-s', tpr_file, '-sys', 'yes'], stdout=subprocess.PIPE, stderr=FNULL)
    tpr_list = result.stdout.decode('utf-8').split("\n")
    found = []
    for item, s in enumerate(substrlist):
        for line in tpr_list:
            if ' '+s+' ' in line:
                value = line.split('=')[1].strip()
                try:
                    found.append(int(value))
                except ValueError:
                    try:
                        found.append(float(value))
                    except ValueError:
                        found.append(value)
    return(found)


''' Finds the time step between two contiguous trajectory records,
    this should be a multiple of the time step used for the simulations
'''
def time_step(tpr_file, traj_type = 'nstxout-compressed'):
    dt, skips = search_setting(tpr_file, substrlist = ['dt', traj_type])
    return(dt*float(skips))



def step2time(step, time_step, start_step: int = 1, start_time: float = 0.):
    return(start_time + time_step*(start_step + step))



def time2step(time, time_step, start_step: int = 1, start_time: float = 0.):
    return( round((t - start_time) / time_step) - start_step )



''' INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY 
    Returns 1 if the distance between residue is greater than limits[0] 
    and lower than limits[1], else, returns 0. (it is elementwise)
'''
def _distance_to_contribution(d, limits):
    if len(limits) == 2: # True for values in range: [ limits[0], limits[1])
        return(   np.where( (d >= limits[0])&(d < limits[1]), 1., 0.)    )



def distance_histogram(dist_file, boundaries, block_size = 500):
    counts =   np.zeros(len(boundaries)-1)
    p_counts = np.zeros(len(boundaries)-1)
    distances = np.load(dist_file, mmap_mode='r')
    n_steps = distances.shape[0]
    step_i = 0
    print("Total number of steps: ", n_steps)
    while step_i < n_steps:
        if (step_i + block_size <= n_steps - 1 ):
            d = np.copy(distances[step_i:step_i+block_size,:])
            print("Computing from step ", step_i, " to step ", step_i+block_size)
        else:
            d = np.copy(distances[step_i:,:])
            print("Computing from step ", step_i, " to step ", n_steps)

        step_i += block_size

        p_counts = np.zeros(len(boundaries)-1)
        for j in range(1,len(boundaries)):
            print("Value ", j, " of ", len(boundaries))
            lim_lower   = boundaries[j-1]
            lim_greater = boundaries[j]
            p_counts[j-1] = _distance_to_contribution(d, (lim_lower,lim_greater) ).sum()
        print(counts)
        counts = counts + p_counts
    return(counts)



#   INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY
#   It allows to compute dipole moments from specified residues
def _dipole_moments_modif(traj, charges, local_indices, molecule_indices, **kwargs):
    local_displacements = md.compute_displacements(traj, local_indices, periodic=True)
    molecule_displacements = md.compute_displacements(traj, molecule_indices, periodic=True)
    xyz = local_displacements + molecule_displacements

    if kwargs['contributions'] is not None:
        contribution = kwargs['contributions']
        return(np.einsum('ijk,j,ij->ik', xyz, charges, contribution))
    else:
        return( xyz.transpose(0, 2, 1).dot(charges) )



''' This routine gets ALL the times readed in trajectory, send them to an array
    WARNING!!! DEPRECATED FUNCTION
'''
def times_to_array(traj_file, top_file, chunk = 500, skip = 0, stride = 1, **kwargs):
    print("WARNING: 'times_to_array' function is deprecated.")
    first_iter = True
    for block in md.iterload(traj_file, top=top_file, chunk=chunk, skip=skip, stride=stride):
        if first_iter:
            times    = block.time
            print(" last time processed: ", times[-1])
            first_iter = False
        else:
            times  = np.concatenate( [times, block.time[:]] )
            print(" last time processed: ", times[-1])
    return(times)



def get_volumes(traj_file, top_file, chunk = 500, skip = 0, stride = 1, **kwargs):
    first_iter = True
    for block in md.iterload(traj_file, top=top_file, chunk=chunk, skip=skip, stride=stride):
        if first_iter:
            volumes    = block.unitcell_volumes
            print(" last time processed: ", block.time[-1])
            first_iter = False
        else:
            volumes  = np.concatenate( [volumes, block.unitcell_volumes[:]] )
            print(" last time processed: ", block.time[-1])
    return(volumes)



# -----------------------------------------------------------------------
#   COMPUTATION OF DIPOLE MOMENTS
# -----------------------------------------------------------------------
#   INPUTS:
#       Mandatory arguments:
#           Arg1, type(str): File name of the trajectory (.xtc) file
#           Arg2, type(str): File name of the coordinate (.gro, ¿.pdb?) file
#           Arg3, type(str or dict): A string with the tpr file name, or a dictionary
#                like those generated by the _charges_from_tpr function
#       Optional arguments:
#           chunk, skip, stride: See the docs. of the MDTraj package.
#           residues, type(str/Numpy array or another iterable): List of residues numbers to 
#               be used in the computation. WARNING: Meaningless results can be obtained!
#           t0, type(float or int): Start time (in ps) [Does not work in conjuntion with 'skip'!]
#           tf, type(float or int): Final time (in ps)
#   OUTPUT:
#       Tuple where first argument is an Numpy array containing the dipolar moment (x,y,z)
#       and the second argument the Numpy array containing the associated times from t0
# Provide the residue name if a distance array is given!
def dipole_moments(traj_file, top_file, charges_from = None, chunk = 500, skip = 0, stride = 1, **kwargs):
    # Create a charges dictionary from the tpr file or used a supplied dictionary
    # (note: keys are atomtypes and values are charges)
    if charges_from == None:
        raise RuntimeError('Charges are unknown!')
    # If an 'str' is supplied, it calls the '_charges_from_tpr' function
    if type(charges_from) is str:
        charges_dic = _charges_from_tpr(str(charges_from))
    # If the 'dic' is supplied, just rename it
    if type(charges_from) is dict:    # OBSOLETO????
        charges_dic = charges_from
    
    charges = _charges_array(charges_from)

    for explore in md.iterload(traj_file, top=top_file, chunk=1):
        # Store the chosen residues in selected_residues
        if 'residues' in kwargs:
            if type(kwargs['residues']) is str:
                selected_residues = []
                for idx, res in enumerate(explore.topology.residues):
                    if res.name == kwargs['residues']:
                        selected_residues.append(idx) 
                selected_atoms = _atoms_of_residues(traj_file, top_file, kwargs['residues'])
            else: # If it is not an string should be an iterable
                selected_residues = kwargs['residues']
               # Indexes of the atoms of the selected_residues
                selected_atoms = _atoms_idx_of_residues_idx(traj_file, top_file, selected_residues)
            charges = charges[selected_atoms]
        else:
            # Get the residues number in the xtc, and select them all
            n_residues = explore.topology.n_residues
            selected_residues = list(range(n_residues))
        # Compute 'local_indices' and 'molecule_indices' for the '_dipole_moments_modif'
        # function (see the original 'dipole_moments' function from the the MDTraj library)
        # For the selected residues, makes a list (and the convert to Numpy Array) of [[index of atom_x, index of the first atom in the residue in which is atom_x] 
        local_indices = np.array([[atom.index ,explore.topology.residue(residue_number).atom(0).index] 
                        for residue_number in selected_residues for atom in explore.topology.residue(residue_number).atoms])
        molecule_indices = np.copy(local_indices)
        molecule_indices[:,0] = 0
        molecule_indices = molecule_indices[:,[1, 0]] 
        break


    # Number of frames in trajectory
    if 'number_of_frames' in kwargs:
        number_of_frames = int(kwargs['number_of_frames'])
    else:
        print("WARNING: As 'number_of_frames' was not provided it will be obtained\n"
            "from the tpr file.")
        aux = search_setting(kwargs['tpr_file'],['nsteps', 'nstxout-compressed'])
        number_of_frames = int(aux[0]/aux[1]) # Maybe is +1 but this way avoids probable annoying issues due to one frame...
        del(aux)

    if 'last_frame' in kwargs:
        try:
            last_frame = int(kwargs['last_frame'])
        except:
            raise RuntimeError('Error in \'last_frame\' argument.')
    else:
        last_frame = number_of_frames - 1 
    # Check consistence of 'last_frame'
    if last_frame >= number_of_frames:
        raise RuntimeError('Error \'last_frame\' must be lower than \'number_of_frames\'.')




    # Numpy array with distances
    if 'distances_file' in kwargs:
        if 'residues' not in kwargs:
            raise RuntimeError("'distances_file' was given but 'residues' was not. ")   

        try:
            # Tuple (distance_array, corresponding_times_array)
            distances = np.load(kwargs['distances_file'], mmap_mode='r')
        except:
            raise RuntimeError('Cannot open distance_file')
        try:
            # Iterable with the limits involved
            limits = kwargs['limits']
        except:
            raise RuntimeError('Invalid or omited "limits" argument.')


    print(" Computing dipole moments:")
    # Number of iteration of the 'for block' loop
    block_iteration = 1
    for block in md.iterload(traj_file, top=top_file, chunk=chunk, skip=skip,stride=stride):

        first_processed_frame = skip + (block_iteration - 1) * chunk
        last_processed_frame  = min(skip + block_iteration * chunk - 1, number_of_frames - 1)
        if not first_processed_frame < last_processed_frame:
            return(m_total)
        items = np.arange(first_processed_frame, min(last_processed_frame, last_frame) + 1 )
        # If a distance file was given
        # Remember, distances.shape is (residues,frames)
        # a[i,j] is the smaller distance of residue i at frame j
        #print("FL   : ", first_processed_frame, last_processed_frame)
        print("Computing from: ", items[0],"to ", items[-1], "of a total of ", number_of_frames)
        if 'distances' in locals():
            # Get the contribution of each ¡residue!
            contrib = _distance_to_contribution(  distances[items,:],  limits)
            # Contributions must be given for each atom and not for each residue
            # beacuse in the a final step atoms*charges are calculated for dipole moments
            # so... Get the contributions of each atom in the residue
            atoms_per_residue = int( len(selected_atoms)/len(selected_residues) )
            contrib = np.repeat(contrib, atoms_per_residue,axis=1)
          
        # If contrib is not defined set it to None
        try:
            contrib
        except NameError:
            contrib = None

        m = _dipole_moments_modif(block,charges,local_indices,molecule_indices, contributions = contrib)

        if block_iteration == 1:
            m_total = m.transpose()
        else:
            m_total = np.concatenate((m_total,m.transpose()), axis=1)
        block_iteration += 1
    return(np.array(m_total)) # <- Check if this is outdated



''' Computes the distance between each molecule of an specified residue kind and the remaining
    residues, and writes them to an array.
    This can take a LONG time. Because of this and the very common power outage the following is done:
    The function works on the entire array (every frame) but can write only to a part of the array.
'''
def distance_analysis(traj_file, top_file, chunk = 500, skip = 0, stride = 1, **kwargs):
    if 'overwrite' in kwargs:
        overwrite = bool(kwargs['overwrite'])
    else:
        overwrite = False     
    
    if 'array_file' not in kwargs:
        raise RuntimeError('You must supply a filename for writting the distances array.\n'
                           'Complete or relative paths are allowed.\n')
    else:
        array_file = kwargs['array_file']
        if os.path.exists(array_file):
            if not overwrite:
                # We do not want to overwrite files accidentally as they can be very costly
                raise RuntimeError("The file already exist. Provide a new file name or set 'overwrite=True'.") 
        elif os.access(os.path.dirname(array_file), os.W_OK):
            pass
        else:
            RuntimeError("Cannot get the required write permissions in the provided path.")
            
    if 'stride' in kwargs:
        raise RuntimeError('This function does not allow "stride" argument.')
    
    # Number of frames in trajectory
    if 'number_of_frames' in kwargs:
        number_of_frames = int(kwargs['number_of_frames'])
    else:
        print("WARNING: 'number_of_frames' was not provided. It will be obtained automatically.\n")
        print("Info: looking for tpr file ...")
        if 'tpr_file' in kwargs:
            print("Info: tpr file found!")
            tpr_file = kwargs['tpr_file']
            try:
                number_of_frames = search_setting(tpr_file, substrlist = ['nsteps'])[0]
            except:
                print("Info: Could not retrieve number_of_frames from TPR. Trying to use trajectory file instead.")
        else:
            print("Info: Tpr file not found.")
            number_of_frames = len(times_to_array(traj_file, top_file, chunk = chunk, skip = 0, stride = 1))



    if 'last_frame' in kwargs:
        try:
            last_frame = int(kwargs['last_frame'])
        except:
            raise RuntimeError('Error in \'last_frame\' argument.')
    else:
        last_frame = number_of_frames - 1 
    # Check consistence of 'last_frame'
    if last_frame >= number_of_frames:
        raise RuntimeError('Error \'last_frame\' must be lower than \'number_of_frames\'.')


    for explore in md.iterload(traj_file, top=top_file, chunk=1):
        # Get the residues number in the xtc
        n_residues = explore.topology.n_residues
       
        if 'chem_specie' in kwargs:
            chem_specie_name = kwargs['chem_specie']
            if not isinstance(chem_specie_name, str):
                raise RuntimeError('Error: "chem_specie" must be a String')
        else:
            raise RuntimeError('You must set the residue name given to the chemical specie of interest')

        list_my_specie = [] # List of residues of 'chem_specie_name' setted by 'chem_specie' argument (by number)
        list_others    = [] # List of remaining residues
        for i,  j in enumerate(explore.topology.residues):
            if j.name == chem_specie_name:
                list_my_specie.append(i)
            else:
                list_others.append(i)
        pairs = list(itertools.product(list_my_specie, list_others))
        break

    # As many columns as distances to be retrieved per frame (eg. number of water molecules)
    cols = len(list_my_specie)
    # First and last frames starting from zero
    first_frame = skip 
    rows = last_frame - first_frame + 1

    # User can supply the desired dtype for the array
    if 'dtype' in kwargs:
        dtype = kwargs['dtype']
    else:
        # Low precision is defaulted in order to conserve space
        dtype = np.float16

    # create array of shape (frames, n_intererst_residues)
    if not overwrite and not os.path.isfile(array_file):
        print("Creating file")
        array_on_disk = np.lib.format.open_memmap(array_file,  dtype=dtype, mode='w+', shape=(rows, cols))
        print("File was created")
    array_on_disk = np.load(array_file, mmap_mode='r+')


    if 'log_file' in kwargs:
        da_log = open(kwargs['log_file'], 'w') # zero avoids accumulte data on buffer and force to write this to disk
        da_log.write('This is a log file describing the advance of \nthe "da_log" function.\n')
        da_log.write(" Computing distances:\n")
        da_log.flush()
        os.fsync(da_log.fileno())
    else:
        print("Log file  was not requested. You should use a log files for longs runs, in order to get info useful for updating the distance files.")
    # Defining the meaning of "distance"
    if 'distance' in kwargs:
        distance = kwargs['distance']
    else:
        # Low precision is defaulted in order to conserve space
        distance = 'closest'
    if distance not in ["ca", "closest", "closest-heavy", "sidechain", "sidechain-heavy"]:
        print("WARNING: Wrong 'distance' argument. Setting it to 'closest'")
        distance = 'closest'

    # Number of iteration of the 'for block' loop
    block_iteration = 1
    for block in md.iterload(traj_file, top=top_file, chunk=chunk, skip=skip, stride=stride):
        # Check http://mdtraj.org/development/api/generated/mdtraj.compute_contacts.html for details
        # Compute distances (returns a tuple  in which the first element has shape=(n_frames, n_pairs) and dtype=np.float32
        distances = md.compute_contacts(block, contacts=pairs, scheme=distance)

        # Array of shape: (n_intererst_residues, n_frames of the block, n_other_residues)
        # Note that (n_frames * n_pairs) = (n_intererst_residues * n_frames * n_other_residues)
        distances = np.array(np.split(distances[0], len(list_my_specie),axis=1)) 

        first_processed_frame = skip + (block_iteration - 1) * chunk
        last_processed_frame  = min(skip + block_iteration * chunk - 1, number_of_frames - 1)
        block_iteration += 1
        items = np.arange(first_processed_frame, min(last_processed_frame, last_frame) + 1 )
        
        # minimum distance of each interest_residue in each frame (n_intererst_residues * n_frames)
        minimum_distances = np.min(distances,axis=2).transpose()
        print(items, items.shape, minimum_distances.shape)
        array_on_disk[items,:] = minimum_distances[list(range(len(items))),:]
        
        if last_frame < last_processed_frame:
                 if 'log_file' in kwargs:
                     da_log.write(" Distance calculation finished.")
                     da_log.close()
                 return(None)
        else:
             if 'log_file' in kwargs:
                 da_log.write("    actually: "+str(last_processed_frame)+"  until "+str(last_frame)+"\n" )     
                 da_log.flush()
                 os.fsync(da_log.fileno())
    return(None)



# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
#   PART 2: OBTAINING AUTOCORRELATION FUNCTION                            *
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 

# -----------------------------------------------------------------------
# Reads the autocorrelation function from XMGrace (XVG) files
# -----------------------------------------------------------------------
# (useful for Gromacs files)
def reads_xvg(s):

    file_xvg = open(s, 'r')
    acf = []

    for n, line in enumerate(file_xvg):
        try:
            acf.append(list(map(float, line.split())))
        except:
            if line.startswith('&'):
                break
            continue
    return( np.array(acf) )

# -----------------------------------------------------------------------
# Compute the cross(or auto)-correlation funcion
# -----------------------------------------------------------------------
#   Computes the cross(or auto)-correlation funcion (CCF). There are different definitions.
#   (see https://en.wikipedia.org/wiki/Cross-correlation#Zero-normalized_cross-correlation_(ZNCC) )
#   Using the default values returns the version commonly used in signal processing
#   (ZNCC in the cited article).
#   For the first defintion in the cited article, you can use:
#   ccf(v1, v2, normalized=False,demean=False)
#
##   INPUTS:
#       Mandatory arguments:
#           Arg1, type(np.ndarray): First function to correlate
#           Arg2, type(np.ndarray): Second function to correlate[1]
#
#       Optional arguments:
#           unbiased, type(bool), False: If true autocovariance denominators are n-k, 
#                                        otherwise n.
#           lags, type(int), len(Arg1)-1: – Number of lags to return.
#           normalized, type(bool), True: Divide cross-correlation by the product of
#                                         the standard deviations of Arg1 and Arg2[2].
#           demaean, type(bool), True: Substracts its mean value to each input array[3].
#           fft, type(bool), True: Uses Fast Fourier Transform. Gives huge speeds up for
#                                  large arrays.
#
#   OUTPUT:
#       Numpy array with the cross(or auto)-correlation funcion
#
#   NOTES:
#       [1] For autocorrelation function use the same array for Arg1 and Arg2.
#           Note that it is not a conmutative operation. If arguments are exchanged
#           the second array must be reversed: cf(a,b)=cf(b,a[::-1])
#       [2] It will return 1 at lag 0 only for autocorrelation.
#       [3] It is done under the signal processing definition of cross-correlation.
#           You probably want this if you are unsure.
def ccf(x, y, unbiased=False, lags=None, normalized=True, demean=True, fft=True):
    nobs = len(x)

    if isinstance(lags, int):
        if lags > nobs - 1:
            raise ValueError('lags must be smaller than nobs - 1')
        if lags < 0:
            raise ValueError('lags value must be greater')

    if lags is None:
        lags = nobs - 1
        #print("Setting lags number to ", lags)

    if not isinstance(lags, int):
        raise ValueError('It was expected an integer value for "lags" (or None)')

    # https://dsp.stackexchange.com/questions/736/how-do-i-implement-cross-correlation-to-prove-two-audio-files-are-similar/739
    from statsmodels.compat.scipy import _next_regular
    """
    From statsmodels.compat.scipy._next_regular function docs:
    Find the next regular number greater than or equal to target.
    Regular numbers are composites of the prime factors 2, 3, and 5.
    Also known as 5-smooth numbers or Hamming numbers, these are the optimal
    size for inputs to FFTPACK.
    Target must be a positive integer.
    """
    # This value helps to speed up the fft computation
    n = _next_regular(2 * nobs + 1)

    if demean:
        x = x - x.mean()
        y = y - y.mean()

    if fft:
        Frf = np.fft.fft(x, n=n)
        Flf = np.fft.fft(y, n=n)
        correl = np.fft.ifft(Frf * np.conjugate(Flf))[:lags+1]
    else:
        correl = np.correlate(x, y, mode='full')[nobs-1:]

    if normalized:
        normalize_factor = np.std(x)*np.std(y)
    else:
        normalize_factor = 1.
        if unbiased:
            raise RuntimeError("'unbiased' only makes sense when 'normalized' is True")

    if unbiased:
        normalize_factor = normalize_factor * (nobs-np.arange(0,nobs))[:lags+1]
    else:
        normalize_factor = normalize_factor * float(nobs)


    return(correl.real/normalize_factor)


# Cross correlation function for vectorial variables.
def ccfND(x, y, coords=None, unbiased=False, lags=None, normalized=True, demean=True, fft=True):

    # For the 1D case, just call the 1D cross correlation function
    if len(x.shape) == 1:
        ccf(x, y, unbiased=unbiased, lags=lags, normalized=normalized, demean=demean, fft=fft) 

    if coords is None:
        # Computation including all directions by default
        coords = np.ones( x.shape[1] )
    else:
        coords = np.array(coords)

    nobs = x.shape[0]
    if lags is None:
        lags = nobs - 1
        print("Setting lags number to ", lags)

    # Media
    mean_x = x.mean(axis=0)
    mean_y = y.mean(axis=0)
    denom = np.sum( (x - mean_x)*(y - mean_y)  , axis=0)
    
    ccf_partial = np.zeros(lags)
    for j in range(x.shape[1]): # Commonly: 3.
        if coords[j] != 0:
            ccf_partial += ccf(x[:,j], y[:,j], unbiased=unbiased, lags=lags, normalized=normalized, demean=demean, fft=fft)[:lags]* denom[j]
    return(ccf_partial / (denom*coords).sum())

def vector_autocorrelate(t_array):
  n_vectors = len(t_array)
  # correlate each component indipendently
  acorr = np.array([np.correlate(t_array[:,i],t_array[:,i],'full') for i in range(3)])[:,n_vectors-1:]
  # sum the correlations for each component
  acorr = np.sum(acorr, axis = 0)
  # divide by the number of values actually measured and return
  acorr = np.array( [ val / (n_vectors - i) for i,val in enumerate(acorr)])
  return((acorr/acorr[0])[:int(len(acorr)/4)])


# -----------------------------------------------------------------------
# Compute the autocorrelation funcion
# -----------------------------------------------------------------------
#   INPUTS:
#       Mandatory arguments:
#           Arg1, type(str): chunk, skip, stride: See the docs. of the MDTraj package.
#       Optional arguments:
#           unbiased, fft, nlags, alpha: See the docs. of the StatsModels package (acf function).
#   OUTPUT:
#       Tuple where first argument is an Numpy array containing the autocorrelation function,
#       the other arguments are the corresponding top and bottom values of the confidence interval
#       ( percent:  (1-alpha)*100. )
def acf(m, **kwargs):
    import statsmodels.tsa.stattools as st
    if not type(m) is np.ndarray:
        m = np.array(m)
    if m.shape[1] == 3:
        m = np.sum(m*m[0,:], axis=1)

    if 'nlags' in kwargs:
        nlags = int(kwargs['nlags'])
    else:
        nlags = int(len(m)/2)

    # Using FFT by default
    if 'fft' in kwargs:
        # Using bool lets use '0' for 'False'
        fft = bool(kwargs['fft'])
    else:
        fft = True

    # Using Biased estimation by default
    if 'unbiased' in kwargs:
        unbiased = bool(kwargs['unbiased'] )
    else:
        unbiased = False

    acf, acf_interval = np.zeros(nlags), np.zeros(nlags)
 
#    for cord in range(m.shape[1]):
    acf_data = st.acf(m, unbiased=unbiased,fft=fft,nlags=nlags,alpha=.05)#[:nlags]
    acf      = acf_data[0][:nlags]
    acf_interval = (acf_data[1][:nlags,1]-acf_data[1][:nlags,0])#**2
#

    return(acf, acf_interval)



# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
#   PART 2: FITTING THE AUTOCORRELATION FUNCTION                          *
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
from scipy.integrate import quad
from scipy.special   import gamma
from scipy.special   import gammaln
from scipy.stats     import gamma as gamma_stats
from scipy.optimize import curve_fit
from scipy import signal


# INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY
# Note: Functions that fails for numpy arrays time inputs were vectorized
# with np.vectorize() in order to be able to use those functions for 
# optimization.
# Mittag-Leffler function
def _ml_mono(x, a):  
    x, a = abs(float(x)), float(a)
    if (x == 0.):   # Avoids a log(0.) error
        return(1.)
    sumation, k = 0., 0.
    while True:
        try:
            this_term = (-1.)**k * np.exp( k*np.log(x) - gammaln(a*k+1.) )
            sumation += this_term
            if   abs(this_term) < 1.E-12:
                return(sumation)
        except:
            return(sumation)
        k += 1.


#   INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY
_ml_vectorized = np.vectorize(_ml_mono)


#   INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY
def _ml(x, a):
    return(_ml_vectorized(x, a))



#   INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY
# Havriliak-Negami series (no closed form in time domain)
def hn_series(t, a1, a2, a3):
    t, a1, a2 = float(t), float(a1), float(a2)
    if (t == 0.):   # Avoids a log(0.) error
        return(1.)
    sumation, k = 0., 0.
    while True:
        try:
            this_term_1 = (-1.)**k * np.exp( gammaln(k + a3) + (a2*(k+a3)) * np.log(t/a1) - gammaln(a2*k+a2*a3+1.) - gammaln(k+1.) )
            sumation += this_term_1       
            if   abs(this_term_1) < 1.E-12:
                return(1. - sumation/gamma(a3) )
        except:
            print(t, a1, a2, a3,k)
            exit()
        k += 1.


#   INTERNAL FUNCION, IT SHOULD NOT BE EXCECUTED DIRECTLY
hn = np.vectorize(hn_series)


#   INTERNAL FUNCIONS, THEY SHOULD NOT BE EXCECUTED DIRECTLY
# Final forms of the fitting functions
def _fit_func_1(t, a1):
#    print("Inside Debye Function, input parmeter = ",a1)
    return (np.exp(-t /a1))


def _fit_func_2(t, a1, a2):
#    print("Kohlrausch", a1, a2)
    return (np.exp(- (t /a1)**a2) )


def _fit_func_3(t, a1, a2):
    return (     _ml( -(t/a1)**a2, a2)  )


def _fit_func_4(t, a1, a2):
    return(1. - gamma_stats.cdf(t/a1,a2))


def _fit_func_5(t, a1, a2, a3):
#    print("Havriliak-Negami",a1, a2, a3)
    return(hn(t, a1, a2, a3))


# Dictionary containing the fitting funtions
fit_funct = {
    "Debye"     : _fit_func_1,
    "Kohlrausch": _fit_func_2, 
    "Cole-Cole" : _fit_func_3, 
    "Cole-Davidson": _fit_func_4,
    "Havriliak-Negami": _fit_func_5
}



# -----------------------------------------------------------------------
# Fits the autocorrelation function to well known models.
# -----------------------------------------------------------------------
#   INPUTS:
#       Mandatory arguments:
#           Arg1, type(str): 


#       Optional arguments:
#           'resample' type(float): Reduces the number of samples through decimation.
#            This allows large time savings for computationally expensive models.
#            Fist a relaxation time (tau) is obtained using the (very fast) Debye model.
#            Then, the original number of samples is divided by the geater integer that
#            gives at least 'resample' samples for each 'tau' frame.
#            Default = 30., Use '0' in order to conserve all the data.
#           'verbose', type(bool): Displays extended information
#   OUTPUT:
#       Tuple where first argument is an Numpy array containing the autocorrelation function,
#       the other arguments are the corresponding top and bottom values of the confidence interval
#       ( percent:  (1-alpha)*100. )

def fit_acf(f, t, **kwargs):
    from inspect import signature

    if len( f ) == 2:
        fx   = f[0]
        conf = f[1]
    else:
        fx   = f
        conf = np.ones(len(fx))
    n = len(fx)
    t    = t[:n]

    conf[0] = 0.001 # Avoids zero division

    if 'verbose' in kwargs:
        verbose = bool(kwargs['verbose'])
    else:
       verbose = False

    # Do a Debye fitting at first for obtaining quicky an approximation of tau
    if 'debye_guess' in kwargs:
        debye_guess = float(kwargs['debye_guess'])
    else:
       debye_guess = 10. # Close to water value

    if verbose:
        print("Fitting to Debye model")
    debye_p = curve_fit(fit_funct["Debye"], t, fx, p0 = debye_guess,sigma = conf)[0]

    # Cut the autocorrelation function up to a significant time
    if 'tf' in kwargs:
        tf = float(kwargs['tf'])
    else:
        tf = "Auto"

    if tf != None:
        if tf == "Auto":
            tf_idx = (np.abs(t- 5.* debye_p)).argmin()
        else:
            tf_idx = (np.abs(t - tf)).argmin()       
        t = t[:tf_idx]
        fx = fx[:tf_idx]
        conf = conf[:tf_idx]
        n = len(fx)
        print(n, tf_idx)
        if verbose:
            print("Last time used during the fitting process: ", t[-1], " ps")

    # Resample to improve performance
    if 'resample' in kwargs:
        resample = float(kwargs['resample'])
    else:
       resample = 30.

    if resample:
        tau_in_t = t[n-1]/ debye_p
        points   = min(tau_in_t*resample, n) # avoids out of range values
        frac = int(float(n) / points) # conservative approach
        fx   = signal.decimate( fx , frac,  n=0)
        t    = np.linspace(t[0],t[-1], num = len(fx), endpoint=True) # equally spaced
        conf = signal.decimate(conf, frac,  n=0)
        if verbose:
            print("Input arrays were resampled.\n"
                  + " ... original length : ", n,
                  "\n ... new length      : ", len(fx))
    if 'models' in kwargs:
        models = kwargs['models']
    else:
        models = ["Debye", "Havriliak-Negami", "Cole-Cole", "Kohlrausch", "Cole-Davidson"]

    fitted_fxs = dict()
    info = ""
    for model in models:
        if verbose:
            print("fitting to model", model, " ...")
        sig = signature(fit_funct[model])
        n_parameters = len(sig.parameters)-1
        guess = np.ones(n_parameters)
        guess[0] = debye_p
        f_params, pcov = curve_fit(fit_funct[model], t, fx, p0=guess, sigma = conf)
        f_fitted = fit_funct[model](t, *f_params)
        fitted_fxs[model] = (f_params, f_fitted, pcov)
        info +="   "+ model + str(f_params)+ "\t\n"

    if verbose:
        print("Results:\n",info)    
    return(t, fitted_fxs)




# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
#   PART 3: CALCULATION OF THE ELECTRICAL PERMITIVITY                     *
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
# *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** ** *** 
# Static permitivity calculation
# Note: Can be used directly form simulations WITHOUT external electric fields
def e_s(mu, volumes, temperature, **kwargs):
    kB    = 1.3806488E-23   # J/K
    e_vac = 8.854187817E-12 # Faraday/m = Coulomb^2 / Joule
    e     = 1.60217662E-19  # Elementary charge in Coulomb

    if 'unbiased' in kwargs:
        unbiased = bool(kwargs['unbiased'])
    else:
        unbiased = True

    mum  = mu.mean(0)            # <M>

    mum2 = mum.dot(mum.T)        # <M>^2
    
    mmum = (mu*mu).mean(0).sum() # <M^2>
 
    if unbiased:
        var = mmum
    else:
        var = mmum - mum2
    
    var = var  * (e*1.E-9)**2  # (Elementary charge * nm)^2 -> (Coulomb * m)^2
    volume = np.mean(volumes) * 1.E-27 # Average system volume nm^3 -> m^3
    e_s = 1.0 + var / (3. * kB * temperature * volume * e_vac)


    if 'variance' in kwargs:
        variance = bool(kwargs['variance'])
    else:
        variance = False
    
    if variance:
        return(e_s, var)
    else:
        return(e_s)


def e_s_v2(mu, volumes, temperature, **kwargs):
    kB    = 1.3806488E-23   # J/K
    e_vac = 8.854187817E-12 # Faraday/m = Coulomb^2 / Joule
    e     = 1.60217662E-19  # Elementary charge in Coulomb

    if 'coords' in kwargs:
        coords = np.array(kwargs['coords'])
    else:
        coords = np.array( [1., 1., 1.] )


    if 'unbiased' in kwargs:
        unbiased = bool(kwargs['unbiased'])
    else:
        unbiased = True


    if 'M2' in kwargs:
        if unbiased == False:
            print("WARNING: 'unbiased' option was set to True in e_s_v2 function")
            unbiased  = True
        mmum = (kwargs['M2'] * coords).sum()
    else: 
        mu   = mu.T * coords # Transpose for compatibility and work only with the selected coords
        mum  = mu.mean(0)            # <M>
        mum2 = mum.dot(mum.T)        # <M>^2
        mmum = (mu*mu).mean(0).sum() # <M^2>



    if unbiased:
        var = mmum
    else:
        var = mmum - mum2

    var = var  * (e*1.E-9)**2  # (Elementary charge * nm)^2 -> (Coulomb * m)^2
    volume = np.mean(volumes) * 1.E-27 # Average system volume nm^3 -> m^3
    e_s = 1.0 + var / (coords.sum() * kB * temperature * volume * e_vac)


    if 'variance' in kwargs:
        variance = bool(kwargs['variance'])
    else:
        variance = False
    
    if variance:
        return(e_s, var)
    else:
        return(e_s)




def mossotti(e_r, N):
    e_vac = 8.854187817E-12 # Faraday/m = Coulomb^2 / Joule
    alpha = 3.*e_vac*(e_r-1.)/(e_r+2)/N
    # N is the number density of the molecules (number per cubic meter), and
    #alpha  is the molecular polarizability in SI-units (C·m2/V).
    return(alpha)



#  H = H0 - y*b
# ESTIMATED MEAN VALUES OF TOTAL DIPOLE MOMENT (m) FROM m (without) an electric
# field E,  and an hypotetical electric field E.
# Output: np.array with [ <M_x>_E, <M_y>_E, <M_z>_E ]
def mu_est1(m, E, temperature, **kwargs):
    kB    = 1.3806488E-23   # J/K
    e     = 1.60217662E-19  # Elementary charge in Coulomb
   
    # Convert to SI
    m = m * (e*1.E-9)  # Elementary charge * nm -> Coulomb * m
    E = E *    1.E9   # V nm^-1 ->  V m

    if 'alpha' in kwargs:
        alpha = kwargs['alpha']
    else:
        alpha = 0.  

    n = float(m.shape[0])

    if 'm_ref' in kwargs:
        m_ref = kwargs['m_ref']*(e*1.E-9)
        m     = m - m.mean(0)
        m_ref = m_ref - m_ref.mean(0)
        DH = (m_ref*E+alpha*E*E) / (kB * temperature)
    else:
        m = m - m.mean(0)#*np.sqrt(n/(n-1)) # lo centro y preservo la varianza
        DH = (m*E+alpha*E*E) / (kB * temperature)

    #print("len m", m.shape, "exp DH mean", np.exp(DH).mean())
    #print("num", (m * np.exp(DH)).mean(0)/(e*1.E-9))
    x  = (m * np.exp(DH)).mean(0) / np.exp(DH).mean()
    return(x/(e*1.E-9))



def mu_another_temp(m, T_0, T_1, H_0, E=0., order=1):
    kB    = 1.3806488E-23   # J/K
    e     = 1.60217662E-19  # Elementary charge in Coulomb
   
    # Convert to SI
    m = m * (e*1.E-9)  # Elementary charge * nm -> Coulomb * m
    E = E *    1.E9   # V nm^-1 ->  V m

    H_0 = H_0 / 6.022140857E23

    m = m - m.mean(0)

    w = np.exp(    H_0*(1./(kB*T_0)-1./(kB*T_1)) + (m*E)/(kB*T_1)                    )


    x  = (m**order * w).mean(0) / w.mean()
    return(x/ (e*1.E-9)**order )



# Estimates the variance of a simulation without electric field, from the dipole moment
# as a function of time from a simulation with electric applied
# REQUIRES THE VALUE OF THE EXTERNAL ELECTRIC FIELD OF THE SIMULATINO
def var_from_field(m, E, temperature, **kwargs):
    kB    = 1.3806488E-23   # J/K
    e     = 1.60217662E-19  # Elementary charge in Coulomb

    n = float(m.shape[1])   
    # Convert to SI
    m = m.T * (e*1.E-9)  # Elementary charge * nm -> Coulomb * m
    E = E * 1.E9   # V /nm ->  V / m

    only_center = np.where( E == 0., 1., 0.) 
 #   print("only center" , only_center)
    m = (m - m.mean(0)*only_center)*np.sqrt(n/(n-1)) # lo centro y preservo la varianza

    DH = (m*E.T).sum(1) / (kB * temperature)
  
    m = m / (e*1.E-9)
    m2 = m*m#/(e*1.E-9)**2

    '''
    Note: here should go    x  = (m2.T * np.exp(DH)).mean(1) / np.exp(DH).mean()
    But np.exp(DH) can leads to errors if the number cannot be represented, so the
    following solution is used
    ''' 
    DH = DH -  DH[np.argmax(abs(DH))]

    print("shapes", m2.shape, m.shape)
    x2  = (m2.T * np.exp(DH)).mean(1) / np.exp(DH).mean()
    x   = (m.T * np.exp(DH)).mean(1) / np.exp(DH).mean()

    return(x2-x**2)



# Computes the dielectric constant for simulations with electric field
def e_s_field(m, E, volumes):
    e_vac = 8.854187817E-12 # Faraday/m = Coulomb^2 / Joule
    e     = 1.60217662E-19  # Elementary charge in Coulomb

    if E == 0.:
        raise RuntimeError('Electric field cannot be zero!')
    # Convert to SI
    if type(m) is np.ndarray:
        m = np.mean(m) * (e*1.E-9) # Elementary charge * nm -> Coulomb * m
    else:
        m = m * (e*1.E-9)
    E = E   / 1.E-9   # V nm^-1 ->  V m
    if type(volumes) is np.ndarray:
        volume = np.mean(volumes) * 1.E-27 # Average system volume nm^3 -> m^3
    else:
        volume  = volumes * 1.E-27 

    return( 1. +  m / (E*volume*e_vac) )



# Extrapolation to E=0 from two points
def e_s_field_zero(m, volumes, temperature, **kwargs):

    # Sets the electric field values for extrapolation    
    if 'E' in kwargs:
        E1, E2 = kwargs['E']
    else:
        E1 = 1.E-3 
        E2 = 3.E-3

    if 'alpha' in kwargs:
        alpha = kwargs['alpha']
    else:
        alpha = 0.

    m1 = mu_est1(m, E1, temperature, alpha = alpha)    
    m2 = mu_est1(m, E2, temperature, alpha = alpha)    
    es_1 = e_s_field(m1, E1, volumes)
    es_2 = e_s_field(m2, E2, volumes)
    e_limit = es_2 - (es_2-es_1)/(E2-E1) * E2
    return(e_limit)



# -----------------------------------------------------------------------
# Compute the frequency dependent permittivity
# -----------------------------------------------------------------------
#   INPUTS:
#       Mandatory arguments:
#           Arg1, type(np.ndarray): Discrete sample of the autocorrelation function
#           Arg2, type(float or int): Step time in ps units
#           Arg2, type(float): Static dielectric permitivity
#   OUTPUT:
#       Tuple where first argument is an Numpy array containing the autocorrelation function,

def epsilon(f, dt, e_s):
   # Frecuencies
   v = np.fft.fftfreq(len(f), float(dt)* 1.E-12)[:-1]# / (2*np.pi)
   # Fourier transform of f derivative with sign changed
   fftd = np.fft.fft( np.diff(-f) )
   eps = ( (e_s - 1.) * fftd + 1. ) 
   # Cut values from negative frequencies
   n = int(len(v)/2)
   return(v[:n], np.real(eps)[:n], -np.imag(eps)[:n])



# Effective number of observations
# Input: x: ACF estimator array
# (optional)   method =  'FTZ'
def n_eff(x, n ,**kwargs):
    
    # Remove the lag 0 of the ACF (where always ACF = 1)
    x = x[1:]

    # Get the total samples
    try:
        n = float(len(n[:,0]))
    except:
        n = float(n)

    # Limiting lag (nc) method
    if 'ncut' in kwargs:
        ncut = kwargs['ncut']
    else:
        ncut = 'FTZ'

    # LIMITING LAG CALCULATION
    # First Transit through Zero method
    # (use only for true ACF greaters than zero, Note: Its common
    #  estimators must take negative values)
    if ncut == 'FTZ':
        nc = np.argmax(x<0.)
    #LSN method (implement?)


    # Method for computing n_eff
    # see eqs. 5, 14, 15 and 16 from Metrol. Meas. Syst., Vol. XVIII (2011), No. 4, pp. 529-542.
    if 'method' in kwargs:
        method = kwargs['method']
    else:
        method = 'simple'

    # Valid for 'standard' and 'Quenouille' estimators of the ACF
    if method == 'simple': # eqs. 5 and 15
        aux = 1. - np.arange(1.,float(nc)+1.) / n
        return( n / (1.+2.*np.sum(aux * x[:nc]) ) )
        
    # Warning,'unbiased' does not return an unbiased estimator
    # Warning, only to be used with the standard estimator for the ACF
    if method == 'unbiased': # eq. 14
        return( n / (1.+2.*np.sum(x[:nc]) ) )

    if method == 'Zieba-Ramza': # eq. 16
        denomin = 1.+2.*np.sum(x[:nc])
        nc = float(nc)
        numerator = n - 2.*nc - 1. + nc*(nc+1.)/n
        return( numerator / denomin + 1.)


# Effective number of degrees of freedom
def v_eff(x, n):
    # Remove the lag 0 of the ACF (where always ACF = 1)
    x = x[1:]

    # Get the total samples
    try:
        n = float(len(n[:,0]))
    except:
        n = float(n)
        
    # LIMITING LAG CALCULATION
    # First Transit through Zero method
    # (use only for true ACF greaters than zero, Note: Its common
    #  estimators must take negative values)
    nc = np.argmax(x<0.)

    v = -1. + n / ( 1. + 2.* np.sum(x[:nc] * x[:nc]) ) 
    
    return(v)



def std_er_field(m, E, volumes, **kwargs):
    import statsmodels.tsa.stattools as st
    e_vac = 8.854187817E-12 # Faraday/m = Coulomb^2 / Joule
    e     = 1.60217662E-19  # Elementary charge in Coulomb

    if E == 0.:
        raise RuntimeError('Electric field cannot be zero!')
    # Convert to SI

    if 'VAR' in kwargs:
        m_std = np.sqrt(kwargs['VAR'])*(e*1.E-9) # Elementary charge * nm -> Coulomb * m
    else:
        if type(m) is np.ndarray:
            m_std = np.std(m) * (e*1.E-9) # Elementary charge * nm -> Coulomb * m
        else:
            raise RuntimeError('The complete array of dipole moments must be given')
    E = E   / 1.E-9   # V nm^-1 ->  V m

    if type(volumes) is np.ndarray:
        volume = np.mean(volumes) * 1.E-27 # Average system volume nm^3 -> m^3
    else:
        volume  = volumes * 1.E-27 
    
    neff = n_eff( st.acf(m, unbiased=True,fft=True,nlags=int(len(m)/2)), len(m) )

    return( m_std/(e_vac*E*volume)/np.sqrt(neff) )



def std_er_fake_field_contrib(m, volumes, T):
    import statsmodels.tsa.stattools as st
    kB    = 1.3806488E-23   # J/K
    e_vac = 8.854187817E-12 # Faraday/m = Coulomb^2 / Joule
    e     = 1.60217662E-19  # Elementary charge in Coulomb

    if type(m) is np.ndarray:
        m_std = np.std(m) * (e*1.E-9)**2 # Elementary charge * nm -> Coulomb * m

    if type(volumes) is np.ndarray:
        volume = np.mean(volumes) * 1.E-27 # Average system volume nm^3 -> m^3
    else:
        volume  = volumes * 1.E-27 
    
    neff = n_eff( st.acf(m, unbiased=True,fft=True,nlags=int(len(m)/2)), len(m) )

    return(m_std/(e_vac*volume*kB*T)/np.sqrt(neff) )
        


def std_e_r(mu, volumes, temperature, **kwargs):
    kB    = 1.3806488E-23   # J/K
    e_vac = 8.854187817E-12 # Faraday/m = Coulomb^2 / Joule
    e     = 1.60217662E-19  # Elementary charge in Coulomb

    volume = np.mean(volumes) * 1.E-27 # Average system volume nm^3 -> m^3

    if 'coords' in kwargs:
        coords = np.array(kwargs['coords'])
    else:
        coords = np.ones( mu.shape[0] )

    mu   = mu.T * coords # Transpose for compatibility and work only with the selected coords
    # WARNING s^4 = s_x ^4 + s_y ^4 + s_z ^4
    mmum = (mu**2).mean(0)  - (mu.mean(0))**2 # M^2 (vector)
    var2 = mmum**2  * (e*1.E-9)**4  # (Elementary charge * nm)^2 -> (Coulomb * m)^2

    if len(mu.shape) > 1:
        var2nu = 0.
        for i in range(mu.shape[1]):
            if coords[i] != 0. :
                acr = ccf(mu[:,i], mu[:,i])
                var2nu += var2[i] / v_eff(acr, len(mu[:,i]))

    else:
        acr = ccfND(mu, mu, coords=coords)
        var2nu = var2 / v_eff(acr, len(mu))


    Var_e_s = 2 * var2nu  / ( (coords.sum() * kB * temperature * volume * e_vac)**2 ) #
    return(np.sqrt(Var_e_s))

