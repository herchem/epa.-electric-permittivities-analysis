## Introduction

EPA is a Python3 library designed for computing dielectric properties from molecular dynamics trajectories.

## Table of Contents
*  [Capabilities](#capabilities)
*  [Installation](#installation)
*   [Usage](#usage)
*   [How to cite?](#how-to-cite)
* [License](#licence)




## Capabilities

Many functions for studying dielectric properties are implemented. The main properties that can be computed using EPA are the following:

* **Dielectric constants**:
  * Fluctuation method, electric field method, and two mixed method.
* **Uncertainties in the computed dielectric constants** for the mentioned methods.
* **Partial electric susceptibilities**:
  * Fluctuation method, electric field method and a mixed method.
* **Uncertainties in the computed partial susceptibilities** for the three methods.
* Cross (and auto) correlation functions.
* **Relaxation times** following standard relaxation models.
  * Debye, Kohlrausch, Cole-Cole, Cole-Davidson, and Havriliak-Negami.
* **Frequency-dependent permittivity**.
* Effective numbers of degrees of freedom and observations.
* Convex hull considering periodic boundary conditions.
* Dielectric properties for molecules between two fixed distances from some structure.


## Installation

The simplest way to use EPA is copying the `epa.py` file into the working directory and import it in the standard way. Eg.

`import epa`

EPA will complain (`ModuleNotFoundError`) if some dependencies (Python libraries) are not installed. Install them on request. 

## Usage

Coming soon.

## How to cite? 

If you use this code, please cite the following articles:

* [Sánchez, H. R. (2019). Uncertainties in the Static Dielectric Constants computed from Molecular Dynamics Simulations. Journal of Molecular Liquids, 111021.](https://doi.org/10.1016/j.molliq.2019.111021)

* [Sánchez, H. R., Irastorza, R. M., & Carlevaro, C. M. (2019). Uncertainties and temperature correction in molecular dynamic simulations of dielectric properties of condensed polar systems. Journal of Molecular Liquids, 278, 546-552.](https://doi.org/10.1016/j.molliq.2019.01.077)


## Acknowledgements

 I want to thank Manuel Carlevaro and Ramiro Irastorza from the [IFLySiB](http://iflysib.unlp.edu.ar/) for their guidance and intellectual contributions. Any error in the source code is solely my responsibility. 



## Licence
**EPA**

Copyright (C) 2019 Hernán Sánchez

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/.

hernan.sanchez.ds@gmail.com
